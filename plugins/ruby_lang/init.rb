
require 'x-exam'

XExam::Plugin.register :ruby_lang do
  name 'Ruby Lang XExam plugin'
  author 'hysios'
  author_url 'http://www.v2ex.com/member/hysios'
  description 'This is a Ruby Lang plugin for XExam'
  version '0.1.0' 
end