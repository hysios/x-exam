class RubyLangController < ApplicationController
  unloadable

  def index
  end

  def show
  end

  def destroy
  end

  def create
  end

  def new
  end

  def edit
  end

  def update
  end
  
end