class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.string :name, :limit => 50
      t.text :description
      t.text :template
      t.decimal :total_score, :precision => 6, :scale => 2
      t.decimal :pass_score, :precision => 6, :scale => 2

      t.timestamps
    end
  end
end
