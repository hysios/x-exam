class CreateSubjectItems < ActiveRecord::Migration
  def change
    create_table :subject_items, :id => false do |t|
      t.integer :subject_id, :limit => 11
      t.integer :question_id, :limit => 11

      t.timestamps
    end
  end
end
