class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :quest_type, :limit => 20
      t.string :section, :limit => 40
      t.text :content
      t.integer :level, :limit => 5
      t.decimal :point, :precision => 6, :scale => 2
      t.string :plugins, :limit => 255
      t.timestamps
    end
  end
end
