class CreateExams < ActiveRecord::Migration
  def change
    create_table :exams do |t|
      t.integer :user_id, :limit => 11
      t.integer :subject_id, :limit => 11
      t.decimal :total_score, :precision => 6, :scale => 2
      t.datetime :start_date
      t.datetime :end_date
    end
  end
end
