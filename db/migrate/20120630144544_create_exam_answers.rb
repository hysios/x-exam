class CreateExamAnswers < ActiveRecord::Migration
  def change
    create_table :exam_answers do |t|
      t.integer :exam_id, :limit => 11
      t.integer :question_id, :limit => 11
      t.string :type, :limit => 20
      t.text :content
      t.decimal :point, :precision => 6, :scale => 2
      t.string :plugins, :limit => 255
      t.timestamps
    end
  end
end
