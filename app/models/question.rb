class Question < ActiveRecord::Base
  # attr_accessible :title, :body

  scope :sections, lambda {|sections| where("section in (?)", sections) }
end
