class Exam < ActiveRecord::Base
  # attr_accessible :title, :body

  scope :for_user, lambda { |user| where("user_id = ?", user.id)}
end
