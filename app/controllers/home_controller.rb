class HomeController < ApplicationController
	before_filter :authenticate_user!
	
	def index 
		@subjects = Subject.all
		@exams = Exam.for_user(current_user)
	end
end
