class SubjectsController < ApplicationController
	before_filter :authenticate_user!
	
	def start
		@subject = Subject.find(params[:id])
		generate_subject_papar(@subject)
		# @subject.
	end

	private 
	def generate_subject_papar(subject)
		render :template => "subjects/_template"
		# subject.tempate
	end
end
