require 'x-exam/builder'

module QuestionsHelper

	class QuestionBuilider

	end

	# question_samples(number, sections..., options)
	#   number: Integer 取出几个样例
	#   sections: Array 节数组
	#   options: Hash 配置选项
	#
	# SAMPLES
	#   <h2>选择题</h2>
	#   <% question_samples(20, "linux 基础知识选择题") do |question| %>
	def question_samples(number, *args, &block)

		options = args.extract_options! || {}

		questions = Question.sections(args).sample(number)

		questions.each_with_index do |quest, i|
			builder = build_question(quest, i)
			builder.render(&block)
		end

		nil
	end

	def build_question(quest, index )
		builder   = case quest.quest_type
					when "single_choice", "multiple_choice", "fill_blank", "programming_simulation"
						name = "quest_#{quest.id}"
						ctnt = YAML.load(quest.content)
						ctnt["point"] = quest.point
						ctnt["index"] = index
						klass = "XExam::Builder::#{quest.quest_type.classify}". constantize
						klass.new(name, ctnt, self)
					else
						plugin = load_plugins(quest.quest_type)
					end

	end

	def load_plugins(type)

	end

	def question(name)
	end
end
