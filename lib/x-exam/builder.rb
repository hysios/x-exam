require 'x-exam/builder/base'

module  XExam
  module Builder
    autoload :SingleChoice, 'x-exam/builder/single_choice'
    autoload :MultipleChoice, 'x-exam/builder/multiple_choice'
    autoload :FillBlank, 'x-exam/builder/fill_blank'
    autoload :ProgrammingSimulation, 'x-exam/builder/programming_simulation'
  end

  autoload :HtmlTagParse, 'x-exam/html_tag'
end

# require 'x-exam/builder/radio'
