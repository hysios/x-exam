module XExam   
  module HtmlTagParse     
    TAGS = %w(a abbr  acronym address applet
area b base basefont bdo big blockquote body br button caption center cite
code col colgroup dd del dfn dir div dl dt em fieldset font form frame
frameset h1 h2 h3 h4 h5 h6 head hr html i iframe img input ins isindex kbd
label legend li link map menu meta noframes noscript object ol optgroup option
p param pre q s samp script select small span strike strong style sub sup
table tbody td textarea tfoot th thead title tr tt u ul var )

    def self.included(klass) 
      TAGS.map do |t|
        # code = <<-RUBY
        #   def #{tag}(*args, &block)
        #     if block_given?
        #       context.tag(#{tag}, *args, &block)
        #     else
        #       context.tag(#{tag}, *args)
        #     end        
        #   end
        # RUBY
        # klass.class_eval code, __FILE__, __LINE__ + 1

        define_method t do |*args, &block|
          if block_given?
            content_tag(t, *args, &block)
          else
            content_tag(t, *args)
          end            
        end
      end
    end

  end
end

