module XExam
  class Plugin
    cattr_accessor :directory
    self.directory = File.join(Rails.root, 'plugins')

    cattr_accessor :public_directory
    self.public_directory = File.join(Rails.root, 'public', 'plugin_assets')

    @registered_plugins = {}

    # class methods
    class << self
      attr_reader :registered_plugins
      private :new

      def def_field(*names)
        class_eval do
          names.each do |name|
            define_method(name) do |*args|
              args.empty? ? instance_variable_get("@#{name}") : instance_variable_set("@#{name}", *args)
            end
          end
        end
      end

      # Plugin constructor
      def register(id, &block)
        p = new(id)
        p.instance_eval(&block)
        # Set a default name if it was not provided during registration
        p.name(id.to_s.humanize) if p.name.nil?

        # Adds plugin locales if any
        # YAML translation files should be found under <plugin>/config/locales/
        ::I18n.load_path += Dir.glob(File.join(p.directory, 'config', 'locales', '*.yml'))

        # Prepends the app/views directory of the plugin to the view path
        view_path = File.join(p.directory, 'app', 'views')
        if File.directory?(view_path)
          ActionController::Base.prepend_view_path(view_path)
        end

        # Adds the app/{controllers,helpers,models} directories of the plugin to the autoload path
        Dir.glob File.expand_path(File.join(p.directory, 'app', '{controllers,helpers,models}')) do |dir|
          ActiveSupport::Dependencies.autoload_paths += [dir]
        end

        registered_plugins[id] = p
      end
          
      def load
        Dir.glob(File.join(self.directory, '*')).sort.each do |directory|
          if File.directory?(directory)
            lib = File.join(directory, "lib")
            if File.directory?(lib)
              $:.unshift lib
              ActiveSupport::Dependencies.autoload_paths += [lib]
            end
            initializer = File.join(directory, "init.rb")
            if File.file?(initializer)
              require initializer
            end
          end
        end
      end
    end

    def_field :name, :description, :url, :author, :author_url, :version, :settings
    attr_reader :id
    # instance methods
    def initialize(id)
      @id = id.to_sym
    end

    def directory
      File.join(self.class.directory, id.to_s)
    end

    def public_directory
      File.join(self.class.public_directory, id.to_s)
    end

    def assets_directory
      File.join(directory, 'assets')
    end

    def <=>(plugin)
      self.id.to_s <=> plugin.id.to_s
    end
  end
end
