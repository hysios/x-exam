require 'x-exam/html_tag'

module XExam
  module Builder

    class Base
      include XExam::HtmlTagParse

      attr_reader :name
      attr_accessor :exam_state

      def initialize(name, ctnt, template)
        @name = name
        generate_attributes(ctnt)
        @context = template

      end

      def generate_attributes(attributes_hash)
        attributes_hash.each do |key,value|
          key = key.gsub /\s/, '_'
          self.class.send(:attr_reader, key.to_sym)
          instance_variable_set "@#{key}".to_sym, value
        end
      end

      def context
        @context
      end

      def render(&block)
        if block_given?
          yield self 
        else
          concat question
        end       
      end

      def on(num)
        I18n.t("question.number", :number => num + 1)
      end

      class << self

        def area(name, &block)

          #extentions = Module.new(&Proc.new) if block_given?
          # extentions.extend XExam::HtmlTagParse
          define_method name, &block
        end
      end

      private 
        def concat(*args)
          context.concat(*args)
        end

        def content_tag(*args)
          context.content_tag(*args)
        end

    end
  end
end