module XExam
  module Builder

  	class ProgrammingSimulation < Base

      area :editor do |lang, source|
        textarea source, :code => lang
      end

      area :title do 
        @description
      end

      area :question do 
        p on(@index) + title
        editor(@language, @source)
      end
		
  	end
   
  end
end
