module XExam
  module Builder

    class SingleChoice < Base
     
      def render(&block)
        if block_given?
          yield self 
        else
          concat content_tag(:p, "#{on(@index)} #{@question}")

          answer_proc = lambda { answers.each {|k,v| render_answer(k,v) }; nil }

          concat content_tag(:p, answer_proc.call)
          concat @correct_answer if exam_state == "scored"
        end
      end

      def render_answer(key, value, &block)
        if block_given?
          # rbuilder = RadioChoiceItem.new(name, key, value, context)
          # rbuilder.render(&block)

          yield RadioChoiceItem.new(name, key, value, context)

        else
          display = "#{key} #{value}"
          aid = "#{name}_#{value.to_s.gsub(/\W+/, '')}"
          concat(context.label_tag(aid, :class=> "radio inline") do 
            concat context.radio_button_tag(name, value) 
            concat display
          end)
        end
        nil
      end
    end

    class RadioChoiceItem

      attr_reader :name, :answer, :value

      def initialize(name, answer, value, context)
        @name  = name
        @answer = answer
        @value = value
        @context = context
      end

      def render(&block)
        yield self if block_given?
      end
    end
  end

end