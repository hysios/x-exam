module XExam
  module Builder

  	class FillBlank < Base
      FILL_REG = /\s+_+/im

      def parse(quest_string)
        s = quest_string.split FILL_REG
        i = 0
        quest_string.gsub FILL_REG do |m|
          i += 1
          context.text_field_tag "#{name}_#{i}" ,nil, :size => m.size * 2, :class => 'fill_blank'
        end
      end

      def question
        parse(@question)
      end

      def render(&block)
        if block_given?
          yield self 
        else
          concat content_tag(:p, "#{on(@index)} #{parse(@question)}" , nil , false)

          concat @correct_answer if exam_state == "scored"
        end
      end

      def render_answer(key, value, &block)
        if block_given?
          # rbuilder = RadioChoiceItem.new(name, key, value, context)
          # rbuilder.render(&block)

          yield CheckboxChoiceItem.new(name, key, value, context)

        else
          display = "#{key} #{value}"
          aid = "#{name}_#{value.to_s.gsub(/\W+/, '')}"
          concat(context.label_tag(aid, :class=> "checkbox inline") do 
            concat context.check_box_tag(name, value, false, {:id => aid}) 
            concat display
          end)
        end
        nil
      end  		
  	end

    class CheckboxChoiceItem

      attr_reader :name, :answer, :value

      def initialize(name, answer, value, context)
        @name  = name
        @answer = answer
        @value = value
        @context = context
      end

      def render(&block)
        yield self if block_given?
      end
    end  	
  end
end
